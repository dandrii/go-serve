package args

import "flag"

var strArgs = map[string]*string{
	"port":   flag.String("port", "5000", ""),
	"config": flag.String("config", "gserve.json", "path to config file"),
}
var boolArgs = map[string]*bool{
	"debug": flag.Bool("debug", false, "bool"),
}

func Bool(key string) bool {
	if value, ok := boolArgs[key]; ok {
		return *value
	}
	return false
}

func String(key string) string {
	if value, ok := strArgs[key]; ok {
		return *value
	}
	return ""
}

func Tail(fallback string) string {
	tail := flag.Arg(0)
	if len(tail) > 0 {
		return tail
	}
	return fallback
}

func init() {
	flag.Parse()
}
