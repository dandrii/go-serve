package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/dandrii/go-serve/args"
)

var config rewrites

type rewrites struct {
	Rewrites []rewrite `json:"rewrites"`
}

type rewrite struct {
	Source string `json:"source"`
	Target string `json:"target"`
}

func initConfig() {
	file, err := os.Open(filepath.Join("./", args.String("config")))

	if err != nil {
		if args.Bool("debug") {
			log.Println("Config file not found, using as static server..")
		}
		config.Rewrites = []rewrite{}
		return
	}

	defer file.Close()

	bytes, _ := ioutil.ReadAll(file)
	json.Unmarshal(bytes, &config)
	if args.Bool("debug") {
		log.Println("Config accepted, redirects:")
		for i := 0; i < len(config.Rewrites); i++ {
			log.Printf("  %s >> %s\n", config.Rewrites[i].Source, config.Rewrites[i].Target)
		}
	}
}

func getListenAddress() string {
	port := args.String("port")
	return ":" + port
}

func getProxyURL(path string) (string, bool) {
	for i := 0; i < len(config.Rewrites); i++ {
		r, _ := regexp.Compile(config.Rewrites[i].Source)
		match := r.FindStringSubmatch(path)
		matchLength := len(match)
		if matchLength > 1 {
			return strings.Replace(config.Rewrites[i].Target, "$1", match[1], 1), true
		}
	}
	return path, false
}

func serveProxy(target string, res http.ResponseWriter, req *http.Request) {
	targetURL, _ := url.Parse(target)

	proxy := httputil.NewSingleHostReverseProxy(&url.URL{
		Host:   targetURL.Host,
		Scheme: targetURL.Scheme,
	})

	req.URL.Host = targetURL.Host
	req.URL.Scheme = targetURL.Scheme
	req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
	req.Host = targetURL.Host

	proxy.ServeHTTP(res, req)
}

func serveSinglePage(target string, res http.ResponseWriter, req *http.Request) {
	path := filepath.Join(args.Tail("./"), filepath.Clean(target))

	_, err := os.Stat(path)

	if err != nil {
		index := filepath.Join(args.Tail("./"), "index.html")
		_, err := os.Stat(index)
		if err != nil {
			http.NotFound(res, req)
			return
		}
		http.ServeFile(res, req, index)
		return
	}

	http.ServeFile(res, req, path)
}

func onRequest(res http.ResponseWriter, req *http.Request) {
	url, ok := getProxyURL(req.URL.Path)
	if args.Bool("debug") {
		log.Printf("%s >> %s\n", req.URL.Path, url)
	}

	if ok {
		serveProxy(url, res, req)
	} else {
		serveSinglePage(url, res, req)
	}
}

func main() {
	initConfig()

	log.Printf("Server will run on: %s\n", getListenAddress())

	http.HandleFunc("/", onRequest)

	if err := http.ListenAndServe(getListenAddress(), nil); err != nil {
		panic(err)
	}
}
